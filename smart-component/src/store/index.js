import {applyMiddleware, combineReducers, compose, createStore} from 'redux'
import logger from 'redux-logger'

import redeemMiddleWare from '../middlewares'
import reducers from '../reducers'

let middlewares = applyMiddleware(
  redeemMiddleWare,
  logger()
);

export default function configureStore(initialState) {
  return compose(middlewares)(createStore)(combineReducers(reducers), initialState);
}