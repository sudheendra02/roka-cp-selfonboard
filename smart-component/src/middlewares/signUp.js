import {Actions,raiseAction} from '../actions'
import {EMAIL_VERIFICATION,OTP_ERROR, SUCCESS} from '../constants'
import {UserProfileClient} from 'player-api-sdk'
import RokaEventBus from 'react-common-utils/dist/utils/RokaEventBus';
let configuration = {};
export function configure(options = {}) {
  Object.keys(options).forEach(option => configuration[option] = options[option])
}
export default async function signUpHandler(store){
    const state = store.getState()
    const {mobileOtp} = state;
    if(!(/^[0-9]{6}$/.test(mobileOtp))){
        store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
            showAlert: true,
            type: 'error',
            message: OTP_ERROR
          }))
    }else{
        const {firstName, lastName, companyEmail, mobile,companyName, legalName} = state;
        let request = {
            name : companyName,
            legalName  : legalName,
            isSobCp : "true",
            currency : "INR",
            partnerUser :   {
            firstName : firstName,
            lastName : lastName,            
            isSobUser : "true",
            mailId : companyEmail,
            phone : mobile
             },
            mobileNumber : mobile,
            otp : mobileOtp
        }
        store.dispatch(raiseAction(Actions.VERIFY_OTP_IN_PROGRESS))
        try{
        const response = await UserProfileClient.verifyMobileOtp(request);
    // const response =  {"httpStatus":"OK","errorCode":null,"status":"otp_verification_failed","errorMessage":null,"data":null}

        // let response = {
        //         httpStatus: "OK",
        //         errorCode: null,
        //         status: "success",
        //         errorMessage: null,
        //         data: {
        //             cpId: "2185",
        //             website: null,
        //             emailId: "shewag1123@gmail.com",
        //             phoneNo: "9011194793",
        //             gstNum: null,
        //             userVerificationStatus: "vendor_registered"
        //         }
        //     }
        if(response && response.status && response.status.toLowerCase() === SUCCESS 
        && response.token
        ){
            store.dispatch(raiseAction(Actions.VERIFY_OTP_COMPLETE))
            store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
                    showAlert: true,
                    type: 'success',
                    message: "Congratulations !! You are successfully registered."
                }))
                $('#self_onboard').modal('hide');
                let data = {
                    permissions : response.permissions,
                    cpId : response.data.cpId,
                    isCPMapped : response.isCPMapped,
                    roleTypes : response.rolesList 
                }
                console.log('configuration :::', configuration)

                configuration.pushState({
                    type: 'SAVE_STORAGE', from: 'roka', loginSuccess: true,
                    from: 'roka',
                    loginSuccess: true,
                    data: data,
                    roleTypes: data.rolesList,
                    sobStatus : 'emailNotVerified',
                    companyName
                  });
                // RokaEventBus.emit('goToselfOnboard',{sobStatus : 'emailNotVerified'})
                // store.dispatch(raiseAction(Actions.SET_CURRENT_VIEW, EMAIL_VERIFICATION))
        }else{
            throw response
        }
    }
    catch(response){
        if (response && response.status && response.status === "otp_verification_failed"){
            store.dispatch(raiseAction(Actions.VERIFY_OTP_FAILED))
            store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
                showAlert: true,
                type: 'error',
                message: "Oops!! You entered a wrong Otp."
              }))
        }else{
        store.dispatch(raiseAction(Actions.VERIFY_OTP_FAILED))
        store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
            showAlert: true,
            type: 'error',
            message: "Something went wrong. Please try again later."
          }))}
    }
    }
    
    
} 