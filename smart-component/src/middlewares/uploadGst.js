import {Actions,raiseAction} from '../actions'
import {ChannelPartnerClient} from 'cp-player-api-sdk'
import {RokaEventBus} from 'react-common-utils'
import { SUCCESS } from '../constants';

export default async function uploadGstHandler(store){
    const state = store.getState()
    const {gstNumber, companyName} = state
    var formData = new FormData();
      var file = $('#uploadGstForm').get(0).dropzone.getAcceptedFiles()[0];
      if (!file || file === null) {
        store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
          showAlert: true,
          type: 'error',
          message: 'Please upload a valid file'
        }));
      } else {
        let arrayOfFiles = []
        arrayOfFiles.push(file)  
        formData.append('file', file);
        formData.append('arrayOfFiles', arrayOfFiles);
        formData.append('isSobCp', 'true')
        formData.append('gstDocPath', null)
        formData.append('partnerId', null)
        formData.append('gstNumber', gstNumber)
        formData.append('companyName', companyName)
        store.dispatch(raiseAction(Actions.UPLOAD_GST_PROCESSING))
        try{
        const response = await ChannelPartnerClient.uploadGstCopy(formData);
        // let response = {status: "success"}
        if(response && response.status && response.status.toLowerCase() === SUCCESS){
            store.dispatch(raiseAction(Actions.UPLOAD_GST_COMPLETE))
            RokaEventBus.emit('RouteToOnboadUsers')
        }else{
          store.dispatch(raiseAction(Actions.UPLOAD_GST_FAILED))
          store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
            showAlert: true,
            type: 'error',
            message: "Something went wrong. Please try again later."
          }))
        }}
        catch(e){
          store.dispatch(raiseAction(Actions.UPLOAD_GST_FAILED))
          store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
            showAlert: true,
            type: 'error',
            message: "Something went wrong. Please try again later."
          }))
        }
      }
}

export async function skipGstHandler(store){
  RokaEventBus.emit('RouteToOnboadUsers')
}