import {Actions, raiseAction} from '../actions'
import {NAME_ERROR, EMAIL_ERROR,
   MOBILE_ERROR, 
   CHECK_TC_ERROR, 
   SUCCESS, MOBILE_VERIFICATION, EMAIL_VERIFICATION, UPLOAD_GST} from '../constants'
import {UserProfileClient} from 'player-api-sdk'

import signUpHandler from './signUp'
import uploadGstHandler, {skipGstHandler} from './uploadGst'
import { ChannelPartnerClient } from 'cp-player-api-sdk';

async function initialize(store, payload){
  const { sobStatus, companyName } = payload;
  const state = store.getState();
  if(companyName){
    store.dispatch(raiseAction(Actions.SET_COMPANY_NAME, companyName))
  }
  switch(sobStatus){
  case 'emailNotVerified':
      store.dispatch(raiseAction(Actions.SET_CURRENT_VIEW, EMAIL_VERIFICATION))
      break;
  case 'gstNotUploaded':
      store.dispatch(raiseAction(Actions.SET_CURRENT_VIEW, UPLOAD_GST))
      break;
  default:
      store.dispatch(raiseAction(Actions.SET_CURRENT_VIEW,"sign-up"))
  }
}


async function continueSignUp(store){
  const state = store.getState();
  const {firstName, lastName, companyEmail, mobile,checkTC,companyName, legalName} = state;
  if(firstName.length === 0 || !(/^[a-zA-Z ]{2,30}$/.test(firstName)) || 
  lastName.length === 0 || !(/^[a-zA-Z ]{2,30}$/.test(lastName))) {
    store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
      showAlert: true,
      type: 'error',
      message: NAME_ERROR
    }))
  }else if(companyEmail.length === 0 || !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(companyEmail))){
    store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
      showAlert: true,
      type: 'error',
      message: EMAIL_ERROR
    }))
  }else if(!(/^[0-9]{10}$/.test(mobile))){
    store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
      showAlert: true,
      type: 'error',
      message: MOBILE_ERROR
    }))
  }else if(checkTC === false){
    store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
      showAlert: true,
      type: 'error',
      message: CHECK_TC_ERROR
    }))
  }else if (companyName.length === 0 || !(/^[a-zA-Z ]{2,30}$/.test(companyName)) ||
  legalName.length === 0 || !(/^[a-zA-Z ]{2,30}$/.test(legalName))){
    store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
      showAlert: true,
      type: 'error',
      message: "Enter a valid details."
    }))
  }
  else{
    store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE, 
      {showAlert: false, type: 'NONE', message: ''}))

    let request = {
      mobileNumber: mobile
    }

    store.dispatch(raiseAction(Actions.CONTINUE_SIGN_UP_PROCESSING))
    try{
    const response = await UserProfileClient.verifyPhoneAndSendOtpForSob(request);
    // const response ={
    //       "status": "SUCCESS",
    //       "errorCode": null,
    //       "errorDesc": null,
    //       "errorLiteral": null,
    //       "isAlreadyVerified": null      
    // }
    if(response && response.status && response.status.toLowerCase() === SUCCESS){
        store.dispatch(raiseAction(Actions.CONTINUE_SIGN_UP_COMPLETE))
        store.dispatch(raiseAction(Actions.SET_CURRENT_VIEW, MOBILE_VERIFICATION))
    }else{
      if(response && response.isAlreadyVerified && response.isAlreadyVerified === "true"){
        store.dispatch(raiseAction(Actions.CONTINUE_SIGN_UP_FAILED))
        store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
          showAlert:true,
          type: 'error',
          message: "This mobile is already registered."
        }))
      }else{
        store.dispatch(raiseAction(Actions.CONTINUE_SIGN_UP_FAILED))
        store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
          showAlert:true,
          type: 'error',
          message: "Something went wrong"
        }))
      }
    }
  }catch(e){
    store.dispatch(raiseAction(Actions.CONTINUE_SIGN_UP_FAILED))
    store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
      showAlert:true,
      type: 'error',
      message: "Something went wrong"
    }))
  }
}
}

async function resendOtpHandler(store){
  const {mobile} = store.getState() 
  let request = {
    mobileNumber: mobile
  }
  store.dispatch(raiseAction(Actions.RESEND_OTP_PROCESSING))
  const response = await UserProfileClient.verifyPhoneAndSendOtpForSob(request)
  if(response && response.status.toLowerCase() === SUCCESS){
    store.dispatch(raiseAction(Actions.RESEND_OTP_COMPLETE))
  }else{
    store.dispatch(raiseAction(Actions.RESEND_OTP_FAILED))
  }
}

async function resendEmail(store){
  const {companyEmail} = store.getState()
  let request = {
    email : companyEmail
  }
  const response = await ChannelPartnerClient.resendEmail(request)
  if(response && response.status && response.status.toLowerCase()){
    store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
      showAlert:true,
      type: 'success',
      message: "Email sent successfully."
    }))
  }else{
    store.dispatch(raiseAction(Actions.SET_ALERT_MESSAGE,{
      showAlert:true,
      type: 'error',
      message: "Failed to send Email."
    }))
  }
}



export default store => next => action => {
  next(action)
  switch(action.type){
    case Actions.COMPONENT_INITIALIZING:
      initialize(store, action.payload)
      break;
    case Actions.CONTINUE_HANDLER:
      continueSignUp(store)
      break;
    case Actions.SIGN_UP_HANDLER:
      signUpHandler(store)
      break;
    case Actions.UPLOAD_GST_HANDLER:
      uploadGstHandler(store)
      break;
    case Actions.RESEND_OTP_HANDLER:
      resendOtpHandler(store)
      break;
    case Actions.SKIP_GST_HSNDLER:
      skipGstHandler(store)
    case Actions.RESEND_EMAIL:
      resendEmail(store)
    default:
      break;
  }
}