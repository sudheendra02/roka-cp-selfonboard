import React, {Component} from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as actions from '../actions'
import {MOBILE_VERIFICATION,EMAIL_VERIFICATION,UPLOAD_GST} from '../constants'


const {Actions, raiseAction} = actions

class App extends Component {

  constructor() {
    super()
  }

  firstNameHandler = value => {
    const {actions:{raiseAction}} = this.props;
    if(/^[a-zA-Z ]{0,30}$/.test(value)){
      raiseAction(Actions.SET_FIRST_NAME, value)
    }
  }

  lastNameHandler = value => {
    const {actions:{raiseAction}} = this.props;
    if(/^[a-zA-Z ]{0,30}$/.test(value)){
    raiseAction(Actions.SET_LAST_NAME, value)
    }
  }

  companyEmailHandler = value => {
    const {actions:{raiseAction}} = this.props;
    raiseAction(Actions.SET_COMPANY_EMAIL, value)
  }

  mobileNumberHandler = value => {
    const {actions:{raiseAction}} = this.props;
    if(/^[0-9 ]{0,10}$/.test(value)){
      raiseAction(Actions.SET_MOBILE_NUMBER, value)
    }
    
  }

  continueHandler = () => {
    const {actions:{raiseAction}} = this.props
    raiseAction(Actions.CONTINUE_HANDLER)
    
  }

  checkTCHandler = (value) => {
    const {actions:{raiseAction}} = this.props
    raiseAction(Actions.SET_CHECK_TC, value)
  }

    
  signUpHandler = () => {
    const {actions:{raiseAction}} = this.props
    raiseAction(Actions.SIGN_UP_HANDLER)
  }

  submitHandler = () => {
    const {actions:{raiseAction}} = this.props
    raiseAction(Actions.SET_CURRENT_VIEW, UPLOAD_GST)
  }
  
  resendEmailHandler = () => {
    const {actions:{raiseAction}} = this.props
    raiseAction(Actions.RESEND_EMAIL)
  }

  mobileOtpHandler = (value) => {
    const {actions:{raiseAction}} = this.props
    if(/^[0-9]{0,6}$/.test(value)){
      raiseAction(Actions.SET_MOBILE_OTP, value)
    }
    
  }

  legalNameHandler = (value) => {
    const {actions:{raiseAction}} = this.props
    raiseAction(Actions.SET_LEGAL_NAME, value)
  }
  
  companyNameHandler = (value) => {
    const {actions:{raiseAction}} = this.props
    raiseAction(Actions.SET_COMPANY_NAME,value)
  }
  
  uploadGstHandler = () => {
    const {actions:{raiseAction}} = this.props
    raiseAction(Actions.UPLOAD_GST_HANDLER)
  }
  
  skipGstHandler = () => {
    const {actions:{raiseAction}} = this.props
    raiseAction(Actions.SKIP_GST_HSNDLER)
  }

  resendOtpHandler = () => {
    const {actions:{raiseAction}} = this.props
    raiseAction(Actions.RESEND_OTP_HANDLER)
  }

  gstNumberHandler = (value) => {
    const {actions:{raiseAction}} = this.props
    if(value.length <= 15){
      raiseAction(Actions.SET_GST_NUMBER, value)
    }
  }

  cancelHandler = () => {
    const {actions:{raiseAction}} = this.props
    $('#self_onboard').modal('hide');
    raiseAction(Actions.SET_CURRENT_VIEW, "sign-up")
    raiseAction(Actions.SET_FIRST_NAME, "")
    raiseAction(Actions.SET_LAST_NAME, "")
    raiseAction(Actions.SET_COMPANY_EMAIL, "")
    raiseAction(Actions.SET_MOBILE_NUMBER, "")
    raiseAction(Actions.SET_CHECK_TC, false)
    raiseAction(Actions.SET_MOBILE_OTP, "")
    raiseAction(Actions.SET_LEGAL_NAME, "")
    raiseAction(Actions.SET_COMPANY_NAME,"")
  }

  closeAlertHandler = () => {
    const {
      actions: {raiseAction}
    } = this.props
    raiseAction(Actions.SET_ALERT_MESSAGE, {
      showAlert: false,
      type: '',
      message: ''
    });
  }
 
  
  render() {
    
    return React.cloneElement(this.props.children, {
      ...this.props,
      firstNameHandler: this.firstNameHandler,
      lastNameHandler: this.lastNameHandler,
      companyEmailHandler: this.companyEmailHandler,
      mobileNumberHandler: this.mobileNumberHandler,
      continueHandler: this.continueHandler,
      signUpHandler: this.signUpHandler,
      submitHandler: this.submitHandler,
      uploadGstHandler: this.uploadGstHandler,
      skipGstHandler: this.skipGstHandler,
      closeAlertHandler: this.closeAlertHandler,
      resendOtpHandler: this.resendOtpHandler,
      checkTCHandler: this.checkTCHandler,
      mobileOtpHandler: this.mobileOtpHandler,
      legalNameHandler: this.legalNameHandler,
      companyNameHandler: this.companyNameHandler,
      resendEmailHandler: this.resendEmailHandler,
      gstNumberHandler: this.gstNumberHandler,
      cancelHandler: this.cancelHandler
    })
  }
}

function mapStateToProps(state, ownProps) {
  return {
    ...ownProps,
    ...state,
  }
}

function mapDispatchToProps(dispatch, ownprops) {
  return {
    actions: bindActionCreators({
      raiseAction
    }, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);