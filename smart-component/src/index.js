import React, {Component} from 'react'
import {Provider} from 'react-redux'
import configureStore from './store'
import {Actions, raiseAction} from './actions'
import Container from './container'
export * from './constants'
import {configure} from './middlewares/signUp'

export default class selfOnboard extends Component{
	constructor(props){
		super(props)
		const {	pushState } = props;
		
		  configure({pushState});
		
		this.appStore = configureStore()
	}

	componentWillMount(){
		const props = {
			...this.props
		}
		this.appStore.dispatch(raiseAction(Actions.COMPONENT_INITIALIZING, props))
	}

	render() {
		return (
			<Provider store ={this.appStore}>
				<Container>
					{this.props.children}
				</Container>
			</Provider>
			)
	}
}
