export const MOBILE_VERIFICATION = "mobile-verification"
export const EMAIL_VERIFICATION = "email-verification"
export const UPLOAD_GST = "upload-gst"
export const NAME_ERROR = "Enter a valid name"
export const EMAIL_ERROR = "Enter a valid Email address"
export const MOBILE_ERROR = "Enter a valid mobile number"
export const CHECK_TC_ERROR = "Accept the Terms and Conditions"
export const SUCCESS = "success"
export const OTP_ERROR = "Enter a valid OTP"