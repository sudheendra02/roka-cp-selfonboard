import { Actions } from '../actions';


function currentView(state = '', action){
    switch (action.type){
        case Actions.SET_CURRENT_VIEW:
            return action.payload
        default:
            return state
    }
}

function legalName(state = '', action){
  switch(action.type){
    case Actions.SET_LEGAL_NAME:
      return action.payload
  default:
    return state
  }
}

function companyName(state = '', action){
  switch(action.type){
    case Actions.SET_COMPANY_NAME:
      return action.payload
    default:
      return state
  }
}

function alerts(state = {showAlert: false, type: 'NONE', message: ''}, action) {
  switch (action.type) {
    case Actions.SET_ALERT_MESSAGE:
      return action.payload;
    default:
      return state;
  }
}

function gstNumber(state = '', action) {
  switch(action.type){
    case Actions.SET_GST_NUMBER:
      return action.payload;
    default:
      return state;
  }
}

function firstName(state = '',action){
  switch(action.type){
    case Actions.SET_FIRST_NAME:
      return action.payload;
    default:
      return state;
  }
}

function lastName(state = '',action){
  switch(action.type){
    case Actions.SET_LAST_NAME:
      return action.payload;
    default:
      return state;
  }
}

function companyEmail(state = '',action){
  switch(action.type){
    case Actions.SET_COMPANY_EMAIL:
      return action.payload;
    default:
      return state;
  }
}

function mobile(state = '',action){
  switch(action.type){
    case Actions.SET_MOBILE_NUMBER:
      return action.payload;
    default:
      return state;
  }
}

function mobileOtp (state = '',action){
  switch(action.type){
    case Actions.SET_MOBILE_OTP:
      return action.payload
    default:
      return state
  }
}

function loaderStatus(state=false,action){
  switch(action.type){
    case Actions.CONTINUE_SIGN_UP_PROCESSING:
    case Actions.RESEND_OTP_PROCESSING:
    case Actions.VERIFY_OTP_IN_PROGRESS:
    case Actions.UPLOAD_GST_PROCESSING:
      return true
    case Actions.CONTINUE_SIGN_UP_COMPLETE:
    case Actions.CONTINUE_SIGN_UP_FAILED:
    case Actions.RESEND_OTP_COMPLETE:
    case Actions.RESEND_OTP_FAILED:
    case Actions.VERIFY_OTP_COMPLETE:
    case Actions.VERIFY_OTP_FAILED:
    case Actions.UPLOAD_GST_COMPLETE:
    case Actions.UPLOAD_GST_FAILED:
      return false
    default:
      return state

  }
}

function checkTC(state = false, action){
  switch(action.type){
    case Actions.SET_CHECK_TC:
      return action.payload
    default:
      return state
  }
}



const rootReducer = {
  currentView,
  legalName,
  alerts,
  companyName,
  firstName, lastName, companyEmail, mobile,
  loaderStatus,
  checkTC,
  mobileOtp,
  gstNumber
}

export default rootReducer
