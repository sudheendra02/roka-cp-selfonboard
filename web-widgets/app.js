import React from 'react'
import ReactDOM from 'react-dom'
import {SessionUtils} from 'react-common-utils'
import {
  UserProfileClientConfigurator
} from '../smart-component/node_modules/player-api-sdk'

import {
  ChannelPartnerClientConfigurator
} from '../smart-component/node_modules/cp-player-api-sdk'


import {SelfOnboard} from './src/index'

function pushState(state) {
  console.log('push state called....');
}

let storage = new SessionUtils();


// const prodBaseUrl = 'https://api.rokahub.com'
const devBaseUrl = 'https://dev-api.rokahub.com'
// const devBaseUrl = 'http://localhost:8089'

UserProfileClientConfigurator(devBaseUrl, storage, null, pushState)
ChannelPartnerClientConfigurator(devBaseUrl, storage, null, pushState)
const props = {}


ReactDOM.render(
  <div className="container">
    <div className="flex-xs-1 flex-sm-2">
      <SelfOnboard {...props}/>
    </div>
  </div>,
  document.getElementById('app-root')
)
