import React from 'react'

const RokaLoader =  (props) => {
  return (
    <div className="roka-loader-wrapper">
      <div className="roka-loader"></div>
    </div>
  )
}

export  default RokaLoader
