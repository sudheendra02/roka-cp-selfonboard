import React, {Component} from 'react'
// import Container from '../../../smart-component/src'
import Container from 'roka-cp-self-onboard-container'
import UI from './ui'

export class SelfOnboard extends Component{
    render() {
        
        return (
            <Container {...this.props}>
                <UI/>
            </Container>
        )
    }
}