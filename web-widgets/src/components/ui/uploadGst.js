import React from 'react'
import DropBox from './dropbox'

export default class UploadGst extends React.Component{
    render(){
        const {uploadGstHandler, skipGstHandler, alerts, 
            closeAlertHandler, companyName, gstNumberHandler,companyNameHandler, gstNumber} = this.props;
        return(
            <div className="flex-wrap flex-col-xs flex-row-sm roka-section panel-body">
                <div className="flex-xs-1 flex-wrap bg-white no-margin flex-center">
                    <div className="flex-xs-1 bg-white no-margin">
                        {
                            alerts.showAlert === true &&
                            <div className={'notification notification--' + alerts.type}>
                            <div className="notification-action">
                                <span className="icon-close" onClick={() => {
                                    closeAlertHandler();
                                }}></span>
                            </div>
                            <div className="notification-message">
                                <strong>{alerts.message}</strong>
                            </div>
                            </div>
                        }
                        <div className="panel-body no-padding">
                            <div className="panel panel-primary">
                                <div className="form-card postcard">
                                    <h3>Upload your GST e-copy</h3>
                                </div>
                            </div>
                            <div className ="row">
                                <div className = "col-xs-12 col-sm-6">
                                    <label htmlFor='gstNumber'>Enter Gst Number :</label>
                                    <input type="text" id="gstNumber" 
                                        className="form-control"
                                        value={gstNumber}
                                        onChange = {(e) => gstNumberHandler(e.target.value)}/>

                                </div>
                                <div className = "col-xs-12 col-sm-6">
                                    <label htmlFor='compyName'>Company Name :</label>
                                    <input type="text" id="compyName" placeholder="Company Name" 
                                        className="form-control"
                                        value={companyName}
                                        onChange = {(e) => companyNameHandler(e.target.value)}/>
                                </div>
                            </div>
                            <span><b>Note : </b> Submit the pdf of your Gst e-copy.</span>
                            {
                                <DropBox/>
                            }
                        </div>
                        <br/>
                        <div className ='row'>
                        <div className="col-xs-12 col-sm-4 pull-left">
                            <button className="btn btn-primary btn-block" type="submit" onClick={()=>{uploadGstHandler()}}>
                            <span className="fa fa-upload"> </span>Upload
                            </button>
                        </div>
                        <div className="col-xs-12 col-sm-4 pull-right">
                            <button className="btn btn-secondary btn-block" type="submit" onClick={()=>{skipGstHandler()}}>
                            Ask me later
                            </button>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}