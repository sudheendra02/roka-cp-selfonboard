import React from 'react'


export default class Signup extends React.Component {
  constructor (props) {
    super(props)
  }
  render() {
      const {
        firstNameHandler, 
        closeAlertHandler, 
        firstName, 
        lastName,
        legalName, 
        legalNameHandler, 
        companyName, 
        companyNameHandler, 
        checkTCHandler, 
        checkTC,
        alerts, 
        lastNameHandler, 
        companyEmailHandler, 
        mobile, 
        mobileNumberHandler, 
        continueHandler,
        companyEmail,
        cancelHandler
    } = this.props;

    
    return(
        <div>
        <div className="flex-xs-1 flex-wrap bg-white no-margin flex-center">
            <div className="flex-xs-1 bg-white no-margin">
                {
                    alerts.showAlert === true &&
                    <div className={'notification notification--' + alerts.type}>
                    <div className="notification-action">
                        <span className="icon-close" onClick={() => {
                            closeAlertHandler();
                        }}></span>
                    </div>
                    <div className="notification-message">
                        <strong>{alerts.message}</strong>
                    </div>
                    </div>
                }
                <div className="panel-body no-padding">
                    
                    {/* <div className= "row">
                    <div className="col-xs-12 col-sm-4">
                    <button className="btn btn-secondary btn-block">&nbsp;Auto fill from Linkedin</button>
                    </div></div> */}
                    <div className="row">
                    <div className="col-xs-12 col-sm-6">
                        <label htmlFor='fname'>First Name : </label>
                        <input type="text" id="fname" value={firstName} placeholder="Enter your first name" className="form-control" onChange = {(e) => firstNameHandler(e.target.value)}/>
                    </div>
                    <div className="col-xs-12 col-sm-6">
                        <label htmlFor='lname'>Last Name : </label>
                        <input type="text" id="lname" value={lastName} placeholder="Enter your last name" className="form-control" onChange = {(e) => lastNameHandler(e.target.value)}/>
                    </div></div><br/>
                    <div className="row">
                    <div className="col-xs-12 col-sm-6">
                        <label htmlFor ='legalName'>Legal Name : </label>
                        <input type="text" id="legalName" value={legalName} placeholder="Enter the legal name" className="form-control" onChange = {(e) => legalNameHandler(e.target.value)}/>
                    </div>
                    <div className="col-xs-12 col-sm-6">
                        <label htmlFor='compyName'>Company Name :</label>
                        <input type="text" id="compyName" value={companyName} placeholder="Enter the company name" className="form-control" onChange = {(e) => companyNameHandler(e.target.value)}/>
                    </div></div><br/>
                    <div className="row">
                    <div className="col-xs-12 col-sm-6">
                        <label htmlFor='CompanyEmail'>Company Email :</label>
                        <input type="text" id="companyEmail" value={companyEmail} placeholder="Company Email" className="form-control" onChange = {(e) => companyEmailHandler(e.target.value)}/>
                    </div>
                    <div className="col-xs-12 col-sm-6">
                        <label htmlFor='mobile'>Mobile Number :</label>
                        <input type="text" id="mobile" placeholder="Mobile Number" className="form-control" value={mobile} onChange = {(e) => mobileNumberHandler(e.target.value)} pattern='[0-9]{0,10}'/>
                    </div></div><br/>
                    <div className = "row">
                    <div className="col-xs-12 col-sm-6">
                        <input
                            name="tc"
                            type="checkbox"
                            checked={checkTC} 
                            onChange = {()=>{
                                checkTCHandler(!checkTC);
                            }}/>&nbsp;
                    I Accept the <a href="" style={{color: 'blue',cursor: 'pointer'}}>Terms and Conditions</a>
                    </div></div><br/>
                </div>
                <div className="row">
                <div className="col-xs-12 col-sm-4 pull-left">
                                <button className="btn btn-secondary btn-block" type="submit" onClick={()=>{cancelHandler()}}>
                                <span className="fa fa-times-circle"> </span>Cancel
                                </button>
                </div>
                <div className="col-xs-12 col-sm-4 pull-right">
                    <button className="btn btn-primary btn-block" type="submit" onClick={() => continueHandler()}>
                    <span className="icon-check"> </span>Continue
                    </button>
                </div></div>
            </div>
        </div>
    </div>
      )
  }
}