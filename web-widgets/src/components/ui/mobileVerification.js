import React from 'react'

export default class MobileVerification extends React.Component{
    render(){
        const {
            resendOtpHandler,
            closeAlertHandler,
            alerts,
            signUpHandler,
            mobileOtpHandler,
            companyEmail,
            mobileOtp,
            firstName, 
            lastName,
            legalName,
            companyName,
            mobile,
            continueHandler,
            mobileNumberHandler,
            cancelHandler
        } = this.props;
        return(
            <div>
                <div className="flex-xs-1 flex-wrap bg-white no-margin flex-center">
                    <div className="flex-xs-1 bg-white no-margin">
                        {
                            alerts.showAlert === true &&
                            <div className={'notification notification--' + alerts.type}>
                            <div className="notification-action">
                                <span className="icon-close" onClick={() => {
                                    closeAlertHandler();
                                }}></span>
                            </div>
                            <div className="notification-message">
                                <strong>{alerts.message}</strong>
                            </div>
                            </div>
                        }
                        <div className="panel-body no-padding">
                            
                            <div className="row">
                                <div className="col-xs-12 col-sm-6">
                                    <label htmlFor='fname'>First Name : </label>
                                    <input type="text" id="fname" value={firstName} placeholder="First Name" className="form-control" disabled/>
                                </div>
                                <div className="col-xs-12 col-sm-6">
                                    <label htmlFor='lname'>Last Name : </label>
                                    <input type="text" id="lname" value={lastName} placeholder="Last Name" className="form-control" disabled/>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12 col-sm-6">
                                    <label htmlFor ='legalName'>Legal Name : </label>
                                    <input type="text" id="legalName" value={legalName} placeholder="Legal Name" className="form-control" disabled/>
                                </div>
                                <div className="col-xs-12 col-sm-6">
                                    <label htmlFor='compyName'>Company Name : </label>
                                    <input type="text" id="compyName" value={companyName} placeholder="Company Name" className="form-control" disabled/>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12 col-sm-6">
                                    <label htmlFor='CompanyEmail'>Company Email :</label>
                                    <input type="text" id = "CompanyEmail" value={companyEmail} placeholder="Company Email" className="form-control" disabled/>
                                </div>
                                <div className="col-xs-12 col-sm-6">
                                    <label htmlFor='mobile'>Mobile Number :</label>
                                    <input type="text" id="mobile" value={mobile} placeholder="Mobile Number" className="form-control" onChange = {(e) => mobileNumberHandler(e.target.value)} pattern='[0-9]{0,10}'/>
                                </div>
                            </div>
                            <div className="row">
                            <div className="col-xs-12 col-sm-5 pull-right">
                                <button className="btn btn-secondary btn-block" type="submit" onClick={()=>{continueHandler()}}>
                                <span className="icon-check"> </span>Send Otp to new mobile
                                </button>
                            </div>
                            </div>                            
                            <div className="row">
                            <div className="col-xs-12 col-sm-6">
                                <label htmlFor='otp'>OTP :</label>
                                <input type="password" id="otp" placeholder="OTP" className="form-control" onChange={(e)=>mobileOtpHandler(e.target.value)} value={mobileOtp}/>
                            </div></div>
                            <span>Please enter the OTP sent to your registered mobile number.</span>
                        </div>
                        <br/>
                        <div>
                        <span style={{color:'blue',cursor:'pointer'}} onClick = {()=>{resendOtpHandler()}}>Resend the OTP</span>
                        </div>
                        <br/>
                        <div className="row">
                        <div className="col-xs-12 col-sm-5 pull-left">
                                <button className="btn btn-secondary btn-block" type="submit" onClick={()=>{cancelHandler()}}>
                                <span className="fa fa-times-circle"> </span>Cancel
                                </button>
                        </div>
                        <div className="col-xs-12 col-sm-5 pull-right">
                            <button className="btn btn-primary btn-block" type="submit" onClick={()=>{signUpHandler()}}>
                            <span className="icon-check"> </span>Sign Up
                            </button>
                        </div>
                        
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}