import React from 'react'
import SignUp from './signUp'
import MobileVerification from './mobileVerification'
import EmailVerification from './emailVerification'
import UploadGst from './uploadGst'
import {RokaLoader} from '../common'

export default class SelfOnboarding extends React.Component {
  constructor (props) {
    super(props)
  }

  render () {
      const {currentView, loaderStatus} = this.props;
      if(loaderStatus){
            <RokaLoader/>
      }
      switch(currentView){
            case "mobile-verification":
                return <MobileVerification {...this.props}/>
            case "email-verification":
                return <EmailVerification {...this.props}/>
            case "upload-gst":
                return <UploadGst {...this.props}/>
            case "sign-up":
                return <SignUp {...this.props}/>
            default:
                return <SignUp {...this.props}/>
      }
  }
}
