import React from 'react'

export default class EmailVerification extends React.Component{
    render(){
        const {submitHandler, alerts, closeAlertHandler, resendEmailHandler} = this.props
        return (
            <div>
            
            <div className="flex-wrap flex-col-xs flex-row-sm roka-section panel-body">
                <div className="flex-xs-1 flex-wrap bg-white no-margin flex-center">
                    <div className="flex-xs-1 bg-white no-margin">
                        {
                            alerts.showAlert === true &&
                            <div className={'notification notification--' + alerts.type}>
                            <div className="notification-action">
                                <span className="icon-close" onClick={() => {
                                    closeAlertHandler();
                                }}></span>
                            </div>
                            <div className="notification-message">
                                <strong>{alerts.message}</strong>
                            </div>
                            </div>
                        }
                        <div className="panel-body no-padding">
                            <div className="panel panel-primary">
                            <h4> Email verification Step</h4>
                            <b>Note : </b>We have sent a verification email to your registered Email Id. Please verify your Email Id by clicking the link in the mail.
                            </div><div onClick={()=>resendEmailHandler()}><span style={{color:'blue',cursor:'pointer'}}>Resend mail</span></div>
                        </div><br/>
                        <div className="col-xs-12 col-sm-4">
                        <button className="btn btn-primary btn-block" type="submit" onClick={()=>{submitHandler()}}>Skip</button></div>
                    </div>
                </div>
            </div>
            </div>
        )
    }
}