import React from 'react';

export default class UploadForm extends React.Component {

  componentDidMount() {
    if (Dropzone) {
      this.myDropzone = new Dropzone('#uploadGstForm', {
        autoProcessQueue: false,
        acceptedFiles: '.pdf',
        addRemoveLinks: true,
        init: function () {
          this.on("addedfile", function () {
            if (this.files.length > 1 && this.files[1] !== null) {
              this.removeFile(this.files[1]);
            }
          });
        }
      });
    }
  }

  render() {

    return (
      <div>
        <div className="section-title">
          <span className="fa fa-upload"></span> Upload the GST e-copy here.
        </div>
        <div className="panel panel-default">
          <div className="form-card postcard">
            <form id="uploadGstForm" action="/file-upload" className="dropzone form-horizontal" encType="multipart/form-data">
              <div className="fallback">
                <input name="file" type="file" id="file" multiple/>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }

}
