var CLIEngine = require('eslint').CLIEngine;
var cli = new CLIEngine({
    fix: true,
    extensions: ['js', 'jsx']
});
for (var i = 0; i < 5; i++) {
    var report = cli.executeOnFiles(['app.js', 'translations.js', 'src/']);
    var errorReport = CLIEngine.getErrorResults(report.results);
    CLIEngine.outputFixes(report);
}
